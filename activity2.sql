
USE record_db;

--a.
SELECT * FROM songs JOIN albums ON songs.album_id = albums.id WHERE songs.genre LIKE "%Funk%";
--b.

SELECT album_title, artists.name FROM albums JOIN artists ON albums.artist_id = artists.id JOIN songs ON songs.album_id = albums.id WHERE songs.length < 300 AND albums.date_released > "2015-12-31";


--c. 
SELECT * FROM albums JOIN artists ON albums.artist_id = artists.id WHERE artists.name LIKE "%o" AND date_released < "2013-01-01";

--d. 
SELECT * FROM songs JOIN albums ON albums.id = songs.album_id JOIN artists ON artists.id = albums.artist_id WHERE songs.length > 300 AND  songs.length < 400 AND albums.artist_id = 1;

--e. 
SELECT * FROM albums JOIN artists ON albums.artist_id = artists.id WHERE album_title LIKE "%born%" AND artists.name LIKE "J%";

--f.
SELECT songs.song_name FROM songs JOIN albums ON albums.id = songs.album_id JOIN artists ON albums.artist_id = artists.id WHERE artists.name LIKE "B%";
--g.
SELECT song_name FROM songs WHERE length > 400;
